CREATE DATABASE  IF NOT EXISTS `personal` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `personal`;
-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: personal
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `entry`
--

DROP TABLE IF EXISTS `entry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `entry` (
  `entryID` varchar(255) NOT NULL,
  `entryName` varchar(255) NOT NULL DEFAULT '',
  `entryEmail` varchar(255) NOT NULL DEFAULT '',
  `entryPhoneNumber` varchar(11) NOT NULL DEFAULT '',
  `entryContent` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`entryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entry`
--

LOCK TABLES `entry` WRITE;
/*!40000 ALTER TABLE `entry` DISABLE KEYS */;
INSERT INTO `entry` VALUES ('16a8a49d-186f-42c8-97de-380b27c6d3eb','Steve Austin','SteveAustin@gmail.com','6472392323','Hey its\' Austin from Global Tech, please call me'),('719cca8d-1f1d-4915-bc7e-2207e650169e','Chris Jericho','chrisjericho@gmail.com','9050443434','Hey Jonathan please call us regarding the job'),('937de25a-06ea-43cb-9bf0-770dd271a44e','Chris Paul','chrispaul@gmail.com','9052444434','Hey Its Chris from Forsys Software , please call me regarding the job'),('c8fe43a2-d9d8-4762-8b8f-21a920206c3b','Jessica Mony','jessicam@gmail.com','6372454434','Jon, Its Jessica from Moneris please call us back');
/*!40000 ALTER TABLE `entry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `portfolio`
--

DROP TABLE IF EXISTS `portfolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `portfolio` (
  `portfolioEmployerID` int unsigned NOT NULL AUTO_INCREMENT,
  `portfolioEmployerContact` varchar(255) NOT NULL DEFAULT '',
  `portfolioEmployerFeedback` varchar(255) NOT NULL DEFAULT '',
  `portfolioEmployerRating` varchar(255) NOT NULL DEFAULT '',
  `entryID` varchar(255) NOT NULL,
  PRIMARY KEY (`portfolioEmployerID`),
  KEY `entryID` (`entryID`),
  CONSTRAINT `portfolio_ibfk_1` FOREIGN KEY (`entryID`) REFERENCES `entry` (`entryID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portfolio`
--

LOCK TABLES `portfolio` WRITE;
/*!40000 ALTER TABLE `portfolio` DISABLE KEYS */;
INSERT INTO `portfolio` VALUES (1,'Hey its Steve Austin, my email is SteveAustin@gmail.com from Global Tech, please contact our team today regarind the job','There is an opening for a Junior Develper as mentioned & we think you would like the role here','We enjoyed your portfolio here would rate it a 7/10','16a8a49d-186f-42c8-97de-380b27c6d3eb'),(2,'Its Chris Jericho from Forsys, my email is chrisjericho@gmail.com calling from Forsys Tech, please contact us today','There is an opening for a Junior  & we think with your work we would like to interview you','Portfolio Looks clean !','719cca8d-1f1d-4915-bc7e-2207e650169e'),(3,'Its Chris Paul again a reminder, my office hours are 9am to 5pm please reach me at : chrispaul@gmail.com','We have a developer position open for you and we are impressed with what we have seen','We could help you polish your portfolio, although Great work','937de25a-06ea-43cb-9bf0-770dd271a44e'),(4,'Jessica from YMCA Tech, my email is jessicam@gmail.com','I think your Portfolio looked Great could you use some improvements but well done ','I give this a B+ , could be more polished with room for improvement','c8fe43a2-d9d8-4762-8b8f-21a920206c3b');
/*!40000 ALTER TABLE `portfolio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resume`
--

DROP TABLE IF EXISTS `resume`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `resume` (
  `resumeEmployerID` int unsigned NOT NULL AUTO_INCREMENT,
  `resumeEmployerName` varchar(255) NOT NULL DEFAULT '',
  `resumeEmployerContact` varchar(255) NOT NULL DEFAULT '',
  `resumeEmployerOpening` varchar(255) NOT NULL DEFAULT '',
  `resumeEmployerDate` date NOT NULL,
  `resumeEmployerFeedback` varchar(255) NOT NULL DEFAULT '',
  `entryID` varchar(255) NOT NULL,
  PRIMARY KEY (`resumeEmployerID`),
  KEY `entryID` (`entryID`),
  CONSTRAINT `resume_ibfk_1` FOREIGN KEY (`entryID`) REFERENCES `entry` (`entryID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resume`
--

LOCK TABLES `resume` WRITE;
/*!40000 ALTER TABLE `resume` DISABLE KEYS */;
INSERT INTO `resume` VALUES (1,'Steve Austin','My email is SteveAustin@gmail.com from Global Tech, please contact our team today regarind the job','There is an opening for a Junior Develper','2021-07-13','We enjoyed your resume','16a8a49d-186f-42c8-97de-380b27c6d3eb'),(2,'Chris Jericho','My email is chrisjericho@gmail.com calling from Forsys Tech, please contact us today','There is an opening for a Junior Develper','2021-08-02','Loved your Work here','719cca8d-1f1d-4915-bc7e-2207e650169e'),(3,'Chris Paul','My office hours are 9am to 5pm please reach me at : chrispaul@gmail.com','We have a developer position open for you','2021-08-02','Nice Resume & Cover Letter','937de25a-06ea-43cb-9bf0-770dd271a44e'),(4,'Jessica Mony','My Nmae is Jessica from YMCA Tech, my email is jessicam@gmail.com','Would you be interested in a Junior Developer Role?','2021-08-03','Went through your work & we are impressed','c8fe43a2-d9d8-4762-8b8f-21a920206c3b');
/*!40000 ALTER TABLE `resume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `userID` varchar(255) NOT NULL,
  `userName` varchar(255) NOT NULL DEFAULT '',
  `userEmail` varchar(255) NOT NULL DEFAULT '',
  `userPassword` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('ca0cadc7-bb4d-4430-9ae3-42bc53622ef0','Jessica Mony','jessicam@gmail.com','$2b$10$cvNVQooHW22ruWwfvHB0NOtWVQMJKlevcJYw90HkXXsAmLcaqUDrO'),('ed31ac23-ded3-4a35-bbb4-5e7763c3bc0a','Steve Austin','SteveAustin@gmail.com','$2b$10$j95PSsfwVDZL58qKm6QKt.TFhePBBXdHXyAjJsSVzlK5cV00rDsxa'),('f3ab1eee-57c3-4a29-99c9-6fdc52fa280f','Chris Jericho','chrisjericho@gmail.com','$2b$10$lyW/d6T.bZoEgmTgAvWUCesF2YhVVCCWHDpWth/s4nKU7ue1WBGbW'),('fad4717e-eb99-488d-9b9c-b78167040852','Chris Paul','chrispaul@gmail.com','$2b$10$7J3jEAVKKWq2Ou1RXNMWTuq3W1JIvvNecewwJFMrjBSi5Tbk9z4FW');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-19 19:00:37
