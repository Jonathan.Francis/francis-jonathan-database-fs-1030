# W21 FS1030 – Jonathan Francis -- PERSONAL PORTFOLIO PROJECT DATABASE


# Links below - for the W21 FS1030 – Jonathan Francis -- PERSONAL PORTFOLIO PROJECT 

*Active - For the Backend API - https://gitlab.com/Jonathan.Francis/francis-jonathan-backend-fs-1030

*Active - For the Frontend  - https://gitlab.com/jonathan-francis/francis_jonathan_frontend_fs1030_localhost

*Active - For the Database - https://gitlab.com/Jonathan.Francis/francis-jonathan-database-fs-1030


# Follow Details below to get MySQL connected to your Back End/Front End Project. See Step 1 & 2 below.

Run the below commands on your workbench or MySQL command line to create a new user for node app

NOTE:
Once Step 1 & Step 2 below are completed and connected with your Back End. You will be able to log into the Front End React Project and create your User Account. Follow the special instructions from the above Front End link on the flow of the website.
Reminder For the login credientials to work , please follow the instructions from the Back End & Database Link above, will need the Back End set up, along with the Database details imported into MySQL

# Step 1 - Choose either Option A for Npm or Option B for Docker.

a. If using npm to connect to MySQL

USE mysql;
CREATE USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost';
FLUSH PRIVILEGES;
Remember the database port will be 3306.

b. If using Docker to connect to MySQL

USE mysql;
CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'newuser'@'localhost';
FLUSH PRIVILEGES;
Remember the database port will be 9003.



# Step 2 - Select either option 1 or 2 (Option 2 is the preferred method)

1. Option 1 - If you choose option 1 here are the steps:

********
a) Enter the following commands:
    CREATE DATABASE personal;
    USE personal;
******** 
b) Please note you may either copy & paste the tables/values from the Back End Project - see link above for Back End Project & copy tables/values from the folder named database/info.sql. In this folder named database you can find the tables/values in the file named info.sql. BUT DO NOT COPY TABLES/VALUES BELOW.

********
2. Option 2 - The preferred method will be to import directly the personal.sql file from this Database Project into your MySQL personal DATABASE.
Once imported directly into the MySQL workbench , enter the following command in MySQL workbench to access the Database and table/values:
USE personal;



********
# NOTES - READ ONLY

**NOTE DO NOT COPY TABLES/VALUES BELOW into MySQL personal DATABASE - for read only - USE personal.sql file in this project to import into MySQL personal DATABASE**


# Table 1 -- Create an Entry

CREATE TABLE IF NOT EXISTS entry (
    entryID VARCHAR(255) NOT NULL PRIMARY KEY,
    entryName VARCHAR(255) NOT NULL DEFAULT '',
    entryEmail VARCHAR(255) NOT NULL DEFAULT '',
    entryPhoneNumber VARCHAR(11) NOT NULL DEFAULT '',
    entryContent VARCHAR(255) NOT NULL DEFAULT ''
);

# Table 1 - Values -- Create an Entry

INSERT INTO entry VALUES
("16a8a49d-186f-42c8-97de-380b27c6d3eb","Steve Austin","SteveAustin@gmail.com","6472392323", "Hey its' Austin from Global Tech, please call me"),
("719cca8d-1f1d-4915-bc7e-2207e650169e","Chris Jericho", "chrisjericho@gmail.com","9050443434", "Hey Jonathan please call us regarding the job"),
("937de25a-06ea-43cb-9bf0-770dd271a44e","Chris Paul", "chrispaul@gmail.com","9052444434", "Hey Its Chris from Forsys Software , please call me regarding the job"),
("c8fe43a2-d9d8-4762-8b8f-21a920206c3b","Jessica Mony", "jessicam@gmail.com","6372454434", "Jon, Its Jessica from Moneris please call us back");



# Table 2 -- Create an User

CREATE TABLE IF NOT EXISTS user (
    userID VARCHAR(255) NOT NULL PRIMARY KEY,
    userName VARCHAR(255) NOT NULL DEFAULT '',
    userEmail VARCHAR(255) NOT NULL DEFAULT '',
    userPassword VARCHAR(255) NOT NULL DEFAULT ''
);


# Table 2 - VALUES -- Create an User

INSERT INTO user VALUES
("ed31ac23-ded3-4a35-bbb4-5e7763c3bc0a","Steve Austin","SteveAustin@gmail.com","$2b$10$j95PSsfwVDZL58qKm6QKt.TFhePBBXdHXyAjJsSVzlK5cV00rDsxa"),
("f3ab1eee-57c3-4a29-99c9-6fdc52fa280f","Chris Jericho", "chrisjericho@gmail.com","$2b$10$lyW/d6T.bZoEgmTgAvWUCesF2YhVVCCWHDpWth/s4nKU7ue1WBGbW"),
("fad4717e-eb99-488d-9b9c-b78167040852","Chris Paul", "chrispaul@gmail.com","$2b$10$7J3jEAVKKWq2Ou1RXNMWTuq3W1JIvvNecewwJFMrjBSi5Tbk9z4FW"),
("ca0cadc7-bb4d-4430-9ae3-42bc53622ef0","Jessica Mony", "jessicam@gmail.com","$2b$10$cvNVQooHW22ruWwfvHB0NOtWVQMJKlevcJYw90HkXXsAmLcaqUDrO");



# Table 3 -- Resume Feedback/Comments

CREATE TABLE IF NOT EXISTS resume (
    resumeEmployerID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    resumeEmployerName VARCHAR(255) NOT NULL DEFAULT '',
    resumeEmployerContact VARCHAR(255) NOT NULL DEFAULT '',
    resumeEmployerOpening VARCHAR(255) NOT NULL DEFAULT '',
    resumeEmployerDate Date NOT NULL,
    resumeEmployerFeedback VARCHAR(255) NOT NULL DEFAULT '',
    entryID VARCHAR(255) NOT NULL ,
    FOREIGN KEY (entryID) REFERENCES entry(entryID) ON DELETE CASCADE,
    PRIMARY KEY (resumeEmployerID)
);


# Table 3 - Values -- Resume Feedback/Comments

INSERT INTO resume VALUES
(1,"Steve Austin","My email is SteveAustin@gmail.com from Global Tech, please contact our team today regarind the job","There is an opening for a Junior Develper","20210713", "We enjoyed your resume","16a8a49d-186f-42c8-97de-380b27c6d3eb"),
(2,"Chris Jericho", "My email is chrisjericho@gmail.com calling from Forsys Tech, please contact us today","There is an opening for a Junior Develper", "20210802","Loved your Work here","719cca8d-1f1d-4915-bc7e-2207e650169e" ),
(3,"Chris Paul", "My office hours are 9am to 5pm please reach me at : chrispaul@gmail.com","We have a developer position open for you","20210802","Nice Resume & Cover Letter","937de25a-06ea-43cb-9bf0-770dd271a44e" ),
(4,"Jessica Mony", "My Nmae is Jessica from YMCA Tech, my email is jessicam@gmail.com","Would you be interested in a Junior Developer Role?","20210803","Went through your work & we are impressed","c8fe43a2-d9d8-4762-8b8f-21a920206c3b");



# Table 4 -- Portfolio Feedback/Rating

CREATE TABLE IF NOT EXISTS portfolio (
    portfolioEmployerID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    portfolioEmployerContact VARCHAR(255) NOT NULL DEFAULT '',
    portfolioEmployerFeedback VARCHAR(255) NOT NULL DEFAULT '',
    portfolioEmployerRating VARCHAR(255) NOT NULL DEFAULT '',
    entryID VARCHAR(255) NOT NULL ,
    FOREIGN KEY (entryID) REFERENCES entry(entryID) ON DELETE CASCADE,
    PRIMARY KEY (portfolioEmployerID)
);

# Table 4 -- Values -- Portfolio Feedback/Rating

INSERT INTO portfolio VALUES
(1,"Hey its Steve Austin, my email is SteveAustin@gmail.com from Global Tech, please contact our team today regarind the job","There is an opening for a Junior Develper as mentioned & we think you would like the role here","We enjoyed your portfolio here would rate it a 7/10","16a8a49d-186f-42c8-97de-380b27c6d3eb"),
(2,"Its Chris Jericho from Forsys, my email is chrisjericho@gmail.com calling from Forsys Tech, please contact us today","There is an opening for a Junior  & we think with your work we would like to interview you", "Portfolio Looks clean !","719cca8d-1f1d-4915-bc7e-2207e650169e" ),
(3,"Its Chris Paul again a reminder, my office hours are 9am to 5pm please reach me at : chrispaul@gmail.com","We have a developer position open for you and we are impressed with what we have seen","We could help you polish your portfolio, although Great work","937de25a-06ea-43cb-9bf0-770dd271a44e" ),
(4,"Jessica from YMCA Tech, my email is jessicam@gmail.com","I think your Portfolio looked Great could you use some improvements but well done ","I give this a B+ , could be more polished with room for improvement","c8fe43a2-d9d8-4762-8b8f-21a920206c3b");



